
public class PortAlreadyInUseException extends Exception{
	int port;
	public PortAlreadyInUseException(int port){
		this.port = port;
	}
	public int what(){
		return port;
	}
}
