import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.util.ArrayList;
import java.util.Random;


public class CTSChamber extends Chamber{
	
	public CTSChamber(int port) throws PortAlreadyInUseException, IOException{
		if (ports.contains(port)){
			throw new PortAlreadyInUseException(port);
		}
		
		serverSocket = new ServerSocket(port);
		
		this.port = port;
		ports.add(port);
		
		frontSeparator = 2;
		endSeparator = 3;
		
		Thread thread = new Thread(this);
		thread.start();
	}

	@Override
	public String toString() {
		return "CTS chamber on port: " + port;
	}

	@Override
	protected void executeCommand(ArrayList<Integer> command) {
		if (clientSocket == null) return;

		OutputStream outstream;
		PrintWriter out;
		ArrayList<Integer> response = new ArrayList<>();
		String helperStr = "";
		String helperStr2 = "";
		
		Random rand = new Random();
	    temperature = rand.nextDouble()*(TEMP_MAX-TEMP_MIN) + TEMP_MIN;
	    humidity = rand.nextDouble()*100.0;
		
		try {
			outstream = clientSocket.getOutputStream();
	    	out = new PrintWriter(outstream,true);

	    	if (command.size() < 3) return;
	    	
	    	int CHK = 0;
	    	for (int i = 0; i < command.size()-1; i++){
	    		CHK ^= command.get(i);
	    	}
	    	
	    	CHK |= 0x80;

	    	if (CHK != command.get(command.size()-1)) return;
	    	
	    	if (command.get(1) == 0xc1){ // A - Analog value read
	    		int channelNumber = (command.get(2)^0x80) - 48;
	    		
	    		switch (channelNumber){
	    		case 0: // Temperature channel
	    			// Calculating Actual value's characters
	    			helperStr = String.format("%05.1f", temperature);
	    			// Calculating Set value's characters
	    			helperStr2 = String.format("%05.1f", 50.0);
	    			break;
	    		case 1: // Humidity channel
	    			// Calculating Actual value's characters
	    			helperStr = String.format("%05.1f", humidity);
	    			// Calculating Set value's characters
	    			helperStr2 = String.format("%05.1f", 50.0);
	    			break;
	    		default:
	    			break;
	    		}
	    		
    			response.add(frontSeparator); // STX
    			response.add(command.get(0)); // ADR
    			response.add(command.get(1)); // A
    			response.add(command.get(2)); // Channel number
    			response.add(0xA0);           // Blank
    			
    			response.add(helperStr.codePointAt(0)|0x80); // Actual value char#1
    			response.add(helperStr.codePointAt(1)|0x80); // Actual value char#2
    			response.add(helperStr.codePointAt(2)|0x80); // Actual value char#3
    			response.add(helperStr.codePointAt(3)|0x80); // Actual value char#4
    			response.add(helperStr.codePointAt(4)|0x80); // Actual value char#5
    			response.add(0xA0);           // Blank
    			
    			response.add(helperStr2.codePointAt(0)|0x80); // Set value char#1
    			response.add(helperStr2.codePointAt(1)|0x80); // Set value char#2
    			response.add(helperStr2.codePointAt(2)|0x80); // Set value char#3
    			response.add(helperStr2.codePointAt(3)|0x80); // Set value char#4
    			response.add(helperStr2.codePointAt(4)|0x80); // Set value char#5
    			
    			// Calculating response
    			int responseCHK = 0;
    			for (int i = 1; i < response.size(); i++){
    				responseCHK ^= response.get(i);
    			}
    			responseCHK |= 0x80;
    			
    			response.add(responseCHK);    // CHK
    			response.add(endSeparator);   // ETX
    			
    			for (int rp : response){
    				outstream.write(rp);
    			}
	    		
	    		
	    	}else{
	    	}
	    	
	    	
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IndexOutOfBoundsException e){
			System.out.println("Illegal command received at: " + toString());
			return;
		}
		
	}
	
}
