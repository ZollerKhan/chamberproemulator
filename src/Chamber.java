import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.omg.CORBA.portable.InputStream;


public abstract class Chamber implements Runnable{
	
	protected static final double TEMP_MIN = -40.0;
	protected static final double TEMP_MAX = 180.0;
	
	
	protected static Set<Integer> ports = new HashSet<Integer>();
	protected int port;
	protected ServerSocket serverSocket;
	protected Socket clientSocket;
	
	private static Set<Chamber> chambers = new HashSet<Chamber>();
	protected ArrayList<Integer> command = new ArrayList<>();
	protected ArrayList<ArrayList<Integer>> commands = new ArrayList<>();
	protected boolean receivingCommand = false;
	protected int frontSeparator;
	protected int endSeparator;
	
	protected boolean run = false;
	
	protected double temperature = TEMP_MIN;
	protected double humidity = 0.0;
	
	private int getPort(){ return port; }
	
	public void run() {
		while (!run){
			try {
				Thread.sleep(10);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		communicate();
		
	}
	
	protected void start(){
		System.out.println("Attaching a chamber on port " + port);
		run = true;
	}
	
	public static boolean startChamber(String chamberType, int port) throws PortAlreadyInUseException, IOException{
		
		Chamber ch;
		
		switch (chamberType){
		case "Weiss":
			ch = new WeissChamber(port);
			chambers.add(ch);
			ch.start();
			break;
		case "CTS":
			ch = new CTSChamber(port);
			chambers.add(ch);
			ch.start();
			break;	
		default:
			System.out.println("Unsupported chamber type!");
			return false;
		}
		
		return true;
	
	}
	
	public static boolean closeChamber(int port){
		if (!ports.contains(port)){
			return false;
		}
		/*
		 * Concurrent modification exception????
		 */
		for (Chamber ch : chambers){
			if (ch.getPort() == port){
				ch.close();
				chambers.remove(ch);
				ports.remove(port);
				break;
			}
		}
		
		return true;
		
	}
	
	protected void close(){
		
		run = false;
		try {
			if (clientSocket != null) clientSocket.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			serverSocket.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("Detaching chamber on port: " + port);
	}
	
	public static void closeAll(){
		for (Chamber ch : chambers){
			ch.close();
		}
	}
	
	protected void communicate() {

		while (run){
			
			try {
				clientSocket = serverSocket.accept();
				
				System.out.println("Just connected to " + toString());
				
				BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
				java.io.InputStream is = clientSocket.getInputStream();
			    int i;
			    while ((i = is.read()) != -1)
			    {
			    	handleInputByte(i);
			    }	
			    
			    	
				
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			
		}
		
	}
	
	public static void listChambers(){
		System.out.println("\nThe following chambers are attached to the system: ");
		for (Chamber ch : chambers){
			System.out.println(ch.toString());
		}
	}
	
	public abstract String toString();
	
	protected void handleInputByte(int input){
		if (input == frontSeparator){
			command.clear();
			receivingCommand = true;
		}else if (input == endSeparator && receivingCommand){
			commands.add(command);
			receivingCommand = false;
		}else{
			command.add(input);
		}
		
		for (Iterator<ArrayList<Integer>> it = commands.iterator(); it.hasNext();){
			executeCommand(it.next());
			it.remove();
			
		}

	}
	
	protected abstract void executeCommand(ArrayList<Integer> command);
	
	
}
