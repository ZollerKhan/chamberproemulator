import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashSet;
import java.util.Set;


public class ChamberEmulator {

	/**
	 * @param args
	 */
	
	static Set<Integer> ports = new HashSet<Integer>();
	
	public static void main(String[] args) {
		System.out.println("Chamber Emulator v1.0");
		System.out.println("Currently supports the following chamber protocols: ");
		System.out.println("      - Weiss (only actual value reading out)");
		System.out.println();
		System.out.println("Usage: ");
		System.out.println("       - to attach a chamber:  start <chamber_type> <port_number>");
		System.out.println("       - to detach a chamber:  close <port_number>");
		System.out.println();
		System.out.println();
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String s;
		while (true){
			try {
				s = br.readLine();
				if (s.equals("quit")){
					Chamber.closeAll();
					break;
				}
				
				
				if (handleCommand(s)){
					Chamber.listChambers();
				}
				
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}
	
	private static boolean handleCommand(String s){
		String[] parts;
		int port;
		String chamberType;
		
		parts = s.split(" ");

		
		
		switch (parts[0]){
		case "start": 	
			
			if (parts.length < 3){
				System.out.println("Insufficient number of arguments!");
				return false;
			}
			
			chamberType = parts[1];	
			try{
				port = Integer.parseInt(parts[2]);
			}catch(NumberFormatException ex){
				System.out.println("Invalid port number!");
				return false;
			}
			
			try {
				return Chamber.startChamber(chamberType,port);
			} catch (PortAlreadyInUseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return false;
			} catch (IOException e){
				e.printStackTrace();
				return false;
			}
			
			
		case "close": 
			
			if (parts.length < 2){
				System.out.println("Insufficient number of arguments!");
				return false;
			}
			
			
			try{
				port = Integer.parseInt(parts[1]);
			}catch(NumberFormatException ex){
				System.out.println("Invalid port number!");
				return false;
			}
			
			
			return Chamber.closeChamber(port);
			
			
		default:
			System.out.println("Invalid arguments!");
		break;				
		}
		
		return false;
		
	}
	
}


