import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Random;


public class WeissChamber extends Chamber{
	
	
	public WeissChamber(int port) throws PortAlreadyInUseException, IOException{
		if (ports.contains(port)){
			throw new PortAlreadyInUseException(port);
		}
		
		serverSocket = new ServerSocket(port);
		
		this.port = port;
		ports.add(port);
		
		frontSeparator = 36;
		endSeparator = 13;
		
		Thread thread = new Thread(this);
		thread.start();
	}
	
	protected void start(){
		System.out.println("Attaching a Weiss chamber on port " + port);
		run = true;
	}

	
	protected void executeCommand(ArrayList<Integer> command){
		
		System.out.print("command ");
		
		if (clientSocket == null) return;
		
		System.out.println("executed");

		OutputStream outstream;
		
		try {
			outstream = clientSocket.getOutputStream();
	    	
	    	if (command.get(2) == 124){
	    		String response = "";
	    		
	    	    Random rand = new Random();
	    	    temperature = rand.nextDouble()*(TEMP_MAX-TEMP_MIN) + TEMP_MIN;
	    	    humidity = rand.nextDouble()*100.0;
	    		
	    		response += String.format("%06.1f", 23.0) + " ";
	    		response += String.format("%06.1f", temperature) + " ";
	    		response += String.format("%06.1f", 50.0) + " ";
	    		response += String.format("%06.1f", humidity) + " ";
	    		response += "0080.0 0080.0 0000.0 0020.0 0000.0 0020.2 0000.0 0020.3 0000.0 0020.4 01101010101010101010101010101010";
				
	    		ArrayList<Integer> responseInt = new ArrayList<>();
	    		
	    		for (int i = 0; i < response.length(); ++i){
	    			responseInt.add(response.codePointAt(i));
	    		}
	    		
	    		responseInt.add(13); // Carriage return <CR>
	    		
	    		for (int rp : responseInt){
    				outstream.write(rp);
    			}

	    		System.out.println(response);
	    	}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IndexOutOfBoundsException e){
			System.out.println("Illegal command received at: " + toString());
			return;
		}
		
	}
	
	public String toString(){
		return "Weiss chamber on port: " + port;
	}

}
